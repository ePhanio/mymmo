package org.komi.mymmo.controllers;

import org.komi.mymmo.entities.BienImmobilier;
import org.komi.mymmo.entities.User;
import org.komi.mymmo.models.BienImmo;
import org.komi.mymmo.services.BienImmobilierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bien_immobilier")
//@SecurityRequirement(name="auth-x")
public class BienImmobilierController {

    @Autowired
    private BienImmobilierService bienImmobilierService;

    /**
     * Liste des biens immobiliers
     * @return
     */
    @GetMapping
    public Iterable<BienImmo> getAll(){
        return bienImmobilierService.getAll();
    }

    /**
     * Recherche un bien par type
     * @param value
     * @return
     */
    @GetMapping("/search_type/{value}")
    public Iterable<BienImmo> searchByType(@PathVariable String value){
        return bienImmobilierService.searchByType(value);
    }

    /**
     * Liste de biens par utilisateur
     * @param user
     * @return
     */
    @GetMapping("/user")
    public Iterable<BienImmobilier> getByUser(@RequestBody User user){
        return bienImmobilierService.getByUseId(user.getId());
    }

    /**
     * enregistre un bien immobilier
     * @param p
     * @return
     */
    @PostMapping
    public BienImmobilier save(@RequestBody BienImmobilier p){
        return bienImmobilierService.save(p);
    }

    /**
     * Supprime un bien
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable Long id){
        return bienImmobilierService.delete(id);
    }
}
