package org.komi.mymmo.controllers;

import org.komi.mymmo.entities.User;
import org.komi.mymmo.models.ResponseObject;
import org.komi.mymmo.models.SignObject;
import org.komi.mymmo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.crypto.dsig.SignedInfo;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public Iterable<User> getUsers(){
        return userService.getUsers();
    }


    @PostMapping("/sign_in")
    public ResponseObject signIn(@RequestBody SignObject request){
        return userService.signIn(request);
    }

    @PostMapping("/sign_up")
    public User signIn(@RequestBody User user){
       return userService.signUp(user);
    }

    @GetMapping
    public void logOut(){

    }

}
