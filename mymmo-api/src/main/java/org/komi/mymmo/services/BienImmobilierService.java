package org.komi.mymmo.services;

import org.komi.mymmo.entities.BienImmobilier;
import org.komi.mymmo.entities.User;
import org.komi.mymmo.models.BienImmo;
//import org.komi.mymmo.repositories.BienImmobilierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BienImmobilierService {
   /* @Autowired
    private BienImmobilierRepository bienImmobilierRepository;*/
    @Autowired
    private UserService userService;

    /**
     * Liste de bien immobiliers
     * @return
     */
    public Iterable<BienImmo> getAll(){
      /*  Iterable<BienImmobilier> biens = bienImmobilierRepository.findAll();
        List data = new ArrayList<>();
        BienImmo bienImmo = new BienImmo();
        for (BienImmobilier current: biens) {
            bienImmo = fabricObject(current);
            data.add(bienImmo);
        }
        return data;*/
        return null;
    }

    /**
     * retourne un objet BienImmo a partir de BienImmobilier
     * @return
     */
    private BienImmo fabricObject(BienImmobilier bien){
        BienImmo b = new BienImmo();
        b.setAdresse(bien.getAdresse());
        b.setDesignation(bien.getDesignation());
        b.setDescription(bien.getDescription());
        b.setLatitude(bien.getLatitude());
        b.setLongitude(bien.getLongitude());
        b.setId(bien.getId());
        b.setPrix(bien.getPrix());
        return b;
    }

    /**
     * Liste de bien immobilier
     * @return
     */
    public Iterable<BienImmobilier> getByUseId(Long uid){
        return null; // bienImmobilierRepository.getByUserId(uid);
    }

    /**
     *
     * @param bienImmobilier
     * @return
     */
    public BienImmobilier save(BienImmobilier bienImmobilier){
        //identify user
       /* try {
            User user =  userService.isAuthenticate(bienImmobilier.getUser().getToken());
            if(user.getId()== bienImmobilier.getUser().getId()){
                return  bienImmobilierRepository.save(bienImmobilier);

            }
        }catch (Exception exception){
            exception.printStackTrace();
        }*/
        return null;
    }

    /**
     * Modifie un bien
     * @param bienImmobilier
     * @return
     */
    public BienImmobilier edit(BienImmobilier bienImmobilier){
       /* try{
            //identify user
            User user =  userService.isAuthenticate(bienImmobilier.getUser().getToken());
            if(user.getId()== bienImmobilier.getUser().getId()){
                return  bienImmobilierRepository.save(bienImmobilier);
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }*/

        return null;
    }

    /**
     * Supprime un bien
     * @param id
     * @return
     */
    public Boolean delete(Long id){
      // BienImmobilier bienImmobilier = bienImmobilierRepository.findById(id).get();
      return null; // delete(bienImmobilier);
    }

    /**
     * Supprime un bien
     * @param bienImmobilier
     * @return
     */
    public Boolean delete(BienImmobilier bienImmobilier){
       /* try {
            //identify user
            User user =  userService.isAuthenticate(bienImmobilier.getUser().getToken());
            if(user.getId()== bienImmobilier.getUser().getId()){
                bienImmobilierRepository.deleteById(bienImmobilier.getId());
                return true;
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }*/
        return false;
    }


    /**
     * Recherche un  bien par type
     * @param value
     * @return
     */
    public Iterable<BienImmo> searchByType(String value) {
        /*Iterable<BienImmobilier> biens =  bienImmobilierRepository.searchByType(value);
        List data = new ArrayList<>();
        BienImmo bienImmo;
        for (BienImmobilier current: biens) {
            bienImmo = fabricObject(current);
            data.add(bienImmo);
        }*/
        return null; // data;
    }
}
