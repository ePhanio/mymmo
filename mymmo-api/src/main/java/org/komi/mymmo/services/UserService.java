package org.komi.mymmo.services;

import org.komi.mymmo.configuration.JwtUtil;
import org.komi.mymmo.entities.User;
import org.komi.mymmo.models.ResponseObject;
import org.komi.mymmo.models.SignObject;
//import org.komi.mymmo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserService implements UserDetailsService {
    /*@Autowired
    private UserRepository userRepository;*/
    @Autowired
    JwtUtil jwtUtil ;
    public Iterable<User> getUsers(){
       return null;//userRepository.findAll();
    }


    /**
     * Authentification
     * @param request
     * @return
     */
    public ResponseObject signIn(SignObject request){
       /* User u = userRepository.getUserByUsername(request.getUsername());
        String hashedPassword = encodePassword(request.getPassword());
        String token ="";
        ResponseObject responseObject = new ResponseObject();
        if(u!=null && u.getPassword()==hashedPassword){
            token = jwtUtil.generateToken(u.getId()+"");
            responseObject.setToken(token);
            return responseObject;
        }*/
        return null;
    }

    /**
     * Encode password
     * @param password
     * @return
     */
    public String encodePassword(String password){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        return hashedPassword;
    }

    /**
     * Enregistrement d'un user
     * @param user
     * @return
     */
    public User signUp(User user){
      /*  User u = userRepository.getUserByUsername(user.getUsername());
        if(u!=null){
            return null;
        }
        String passwordEncode = encodePassword(user.getPassword());
        user.setPassword(passwordEncode);
        return  userRepository.save(user);*/
        return  null;
    }

    public User editUser(User user){
        return null;
    }

    /**
     * Logout
     */
    public void logOut(){

    }

    /**
     * Verifie si l'utilisateur s'est authentifie
     * @return
     */
    public User isAuthenticate(String token){
      /*  String uid = jwtUtil.getUidFromToken(token);
        Long id = Long.parseLong(uid);
        User u = userRepository.findById(id).get();
        if(u!=null){
            return u;
        }*/
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       // User user = userRepository.getUserByUsername(username);
        User user = new User();
        user.setUsername("admin");
        user.setPassword("admin");
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }

    public int add(int a, int b){
        return a+b;
    }
}
