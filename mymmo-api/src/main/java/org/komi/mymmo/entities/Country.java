package org.komi.mymmo.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import nonapi.io.github.classgraph.json.Id;

//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;

@Data
@NoArgsConstructor
//@Entity
public class Country {
    // // @Id
    // @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String nom;
    private String code;

}
