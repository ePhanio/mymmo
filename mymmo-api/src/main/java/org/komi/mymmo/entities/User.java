package org.komi.mymmo.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;

//import javax.persistence.*;
import java.util.Collection;

@Data
@NoArgsConstructor
//@Entity(name = "Users")
//@Table(name = "users")
public class User  {
    //   @Id
    //   @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String nom;
    private String email;
    private String username;
    // @JoinColumn(nullable = true)
    private String adresse;
    private String telephone;
    //   @JoinColumn(nullable = true)
    private Double latitude;
    //  @JoinColumn(nullable = true)
    private Double longitude;
    private String ville;
    private String deviceId;
    private String password;
    //   @ManyToOne
    private Country country;
    //  @Transient
    private String token="";



}
