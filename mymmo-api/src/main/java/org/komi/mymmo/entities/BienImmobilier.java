package org.komi.mymmo.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import nonapi.io.github.classgraph.json.Id;

//import javax.persistence.*;

@Data
@NoArgsConstructor
//@Entity
public class BienImmobilier {
    //@Id
    //@GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String designation;
    private String adresse;
    private Double latitude;
    private Double longitude;
    private Double prix;
    private String description;
    // @ManyToOne(fetch = FetchType.EAGER)
    private TypeBienImmobilier typeBienImmobilier;
    //@ManyToOne(fetch = FetchType.EAGER)
    private User user;


}
