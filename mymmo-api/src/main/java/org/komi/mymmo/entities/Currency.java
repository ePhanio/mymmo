package org.komi.mymmo.entities;

public enum Currency {
    EURO("Euro"),
    DOLLAR("Dollar");
    final String value ;

    Currency(String value) {
        this.value = value;
    }

    
    public String getValue() {
        return value;
    }
}
