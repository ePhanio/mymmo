package org.komi.mymmo.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BienImmo {
    private Long id;
    private String designation;
    private String adresse;
    private Double latitude;
    private Double longitude;
    private Double prix;
    private String description;


}
