package org.komi.mymmo.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SignObject {
    private String password;
    private String username;
    private String deviceId;

}
