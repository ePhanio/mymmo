/*package org.komi.mymmo.repositories;

import org.komi.mymmo.entities.BienImmobilier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface BienImmobilierRepository extends CrudRepository<BienImmobilier,Long> {
    @Query("select b from BienImmobilier b where b.user=:uid")
    public Iterable<BienImmobilier> getByUserId(@Param("uid") Long uid);

    @Query("select b from BienImmobilier b where lower(b.typeBienImmobilier.designation) like lower(concat('%', :valueToFind,'%'))")
    public Iterable<BienImmobilier> searchByType(@Param("valueToFind") String value);
}*/
