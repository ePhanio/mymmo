package org.komi.mymmo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import org.jetbrains.annotations.NotNull;
import org.komi.mymmo.model.BienImmo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String URL ="http://10.8.110.230:8080/bien_immobilier";

    private static final  String TAG = "MapsActivity";
    private GoogleMap mMap;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;

    private double latitude=48.8534 ;
    private double longitude=2.3488;
    private String descriptionPosition="Position par defaut";
    private static final float DEFAULT_ZOOM = 11.0f;

    private Boolean mLastLocationPermissionGranted = false;
    private List<BienImmo> listBiens = new ArrayList();
    private EditText searchText;
    private Button btnSearch;

    private Toolbar mTopToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mTopToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mTopToolbar);

        initMap();
       // getProperties();
       // getLocationPermission();
       // updateLocationUI();
      //  initMap();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.action_signin:
                Intent intent =  new Intent(getApplicationContext(), SignInActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i("tttxxxxxxxxxxxxxxxxxxxxxx","colll22222");
        Toast.makeText(this,"Map déjà chargé!!", Toast.LENGTH_LONG);
        mMap = googleMap;
        getCurrentPossition();
        //update and settings the map
        updateUiSettings();

       /* Geocoder geoCoder = new Geocoder(this, Locale.getDefault());

        List<Address> addresses = null;
        String address="";
        try {
            addresses = geoCoder.getFromLocation(latitude, longitude, 1);
            Log.i("opxxxxxxxxxxxxxxxxxx","xillllll");
          //  address = addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            Log.e("IOEXception", e.getMessage());
        }

        Log.i("Adsressxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", address);*/

        //charger the properties from the api
        getBienImmobiliers();
        searchText = findViewById(R.id.input_search);
        btnSearch = findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(A ->{
            String valueInput="";
            valueInput = searchText.getText().toString();
            if(!valueInput.isEmpty()){
                rechercheBienImmobilier(valueInput);
            }
        });
    }

    /**
     * markeur a la position currente
     */
    public void initMyLocationMarker(){
        if(mMap!=null){
            Log.i("xxxxxxxxxxxxxx2", latitude+"");
            LatLng latLng = new LatLng(latitude, longitude);
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng).draggable(false).title(descriptionPosition).snippet(descriptionPosition);
            mMap.addMarker(markerOptions);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng , DEFAULT_ZOOM));
        }
    }

    /**
     * Recherche de bien immobilier
     * @param value
     */
    private void rechercheBienImmobilier(String value) {
        OkHttpClient client =  new OkHttpClient.Builder()
                .build();
        Request request = new Request.Builder()
                .url(URL+"/search_type/"+value)
                .build();
        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    Log.i("Failure request search", e.getMessage());
                }
                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    ObjectMapper objectMapper = new ObjectMapper();
                    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    listBiens = objectMapper.readValue(response.body().string(), new TypeReference<List<BienImmo>>(){});
                    Log.i("resqqqqqqqqqq", listBiens.size()+"");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(mMap!=null){
                                mMap.clear();
                                initMyLocationMarker();
                                for(BienImmo currentData: listBiens) {
                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(currentData.getLatitude(), currentData.getLongitude()))
                                            .anchor(0.5f,0.5f)
                                            .title(currentData.getDescription()+", "+currentData.getPrix())
                                            .snippet(currentData.getAdresse())
                                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.m_red)) );
                                }
                            }
                        }
                    });
                }
            });

        }catch (Exception e) {
            Log.v(TAG,"Erreur reseau"+e);
        }
    }

    /**
     * retourne les bien immobiler a partir de l' api
     */
    public void getBienImmobiliers(){
        OkHttpClient client =  new OkHttpClient.Builder()
                .build();
        Request request = new Request.Builder()
                .url(URL)
                .build();
        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    Log.i("Failurexxxxxxxxxx", e.getMessage());
                }
                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    ObjectMapper objectMapper = new ObjectMapper();
                    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    listBiens = objectMapper.readValue(response.body().string(), new TypeReference<List<BienImmo>>(){});
                    Log.i("resqqqqqqqqqq", listBiens.size()+"");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(mMap!=null){
                                for(BienImmo currentData: listBiens) {
                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(currentData.getLatitude(), currentData.getLongitude()))
                                            .anchor(0.5f,0.5f)
                                            .title(currentData.getDescription()+", "+currentData.getPrix())
                                            .snippet(currentData.getAdresse())
                                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.m_red)) );
                                }
                            }
                        }
                    });
                }
            });

        }catch (Exception e) {
            Log.v(TAG,"Erreur reseau"+e);
        }
    }


    /**
     * initialisation du map
     */
    public void initMap(){
        SupportMapFragment mapFragment= (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void updateUiSettings() {
        Log.i("UPDATE_UI", "update ui");
        if (mMap == null) {
            return;
        }
        try {
            //show the zoom controls.
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);

            mMap.setMyLocationEnabled(true);
            //mMap.getUiSettings().setMyLocationButtonEnabled(true);

        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * Current position
     */
    private void getCurrentPossition(){
        try {
            getLocationPermission();
            if(!mLastLocationPermissionGranted){
                initMyLocationMarker();
                return;
            }
            Task<Location> locationResult = LocationServices.getFusedLocationProviderClient(this).getLastLocation();
            locationResult.addOnCompleteListener((Task<Location> task) ->{
                Location location = task.getResult();

                if(location!=null){
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    descriptionPosition = "Ma position actuelle";
                }
                initMyLocationMarker();
                Log.i("xxxxxxxxxxxxxxx",latitude+"");
            });
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage(), e);
        }
    }

    /**
     * Permission pour la localisation de l'appareil
     */
    public void getLocationPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        int selfPermissionStep1 = ContextCompat.checkSelfPermission(this, FINE_LOCATION);
        int selfPermissionStep2 = -1;
        if (selfPermissionStep1 == PackageManager.PERMISSION_GRANTED) {
            selfPermissionStep2 = ContextCompat.checkSelfPermission(this.getApplicationContext(), COARSE_LOCATION);
            if (selfPermissionStep2 == PackageManager.PERMISSION_GRANTED){
                 this.mLastLocationPermissionGranted=true;
            }else{
                ActivityCompat.requestPermissions(this,permissions,LOCATION_PERMISSION_REQUEST_CODE);
            }
        }else{
            ActivityCompat.requestPermissions(this,permissions,LOCATION_PERMISSION_REQUEST_CODE);
        }
        Log.i("GET_LOCATION", "fin get location");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        this.mLastLocationPermissionGranted = false;
        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if(grantResults.length>0 ){
                     for(int i=0; i<grantResults.length;i++){
                         if(grantResults[i] == PackageManager.PERMISSION_GRANTED){
                             mLastLocationPermissionGranted = true;
                             break;
                         }
                    }
                }
            }
        }

    }

}
