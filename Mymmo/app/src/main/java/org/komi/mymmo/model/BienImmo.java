package org.komi.mymmo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BienImmo {
    private Long id;
    private String designation;
    private String adresse;
    private Double latitude;
    private Double longitude;
    private Double prix;
    private String description;
    /*@JsonProperty("typeOfProperty")
    private TypeOfProperty typeOfProperty;
    @JsonProperty("user")
    private PropertyOwner owner;*/

    public Long getId() {
        return id;
    }

    public String getDesignation() {
        return designation;
    }

    public String getAdresse() {
        return adresse;
    }

    public Double getPrix() {
        return prix;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getDescription() {
        return description;
    }

}
