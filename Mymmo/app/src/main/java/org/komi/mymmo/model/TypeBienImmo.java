package org.komi.mymmo.model;

public class TypeBienImmo {
    private Long id;
    private String designation;

    public Long getId() {
        return id;
    }

    public String getDesignation() {
        return designation;
    }
}
