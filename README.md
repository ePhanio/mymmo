# Mymmo


Appli de réseau spécialisé pour les agents immobiliers. Ils vont pouvoir créer leurs biens, les localiser, et ensuite les mettre disponible à la consultation du grand public.

## Requirements
Java11
Postgresql

## How to setup

### Backend

#### Base de donnée: Postgresql 
```
Création de la base de donnée
Nom de la base de donnée : mymmo_db 
Ensuite restorer les donnée de test de biens immobiliers pour l'affichage sur la carte dans le dossier : db/mymmo_db 
```
#### Appli
```
cd mymmo-api
mvn clean
mvn package
cd target
java -jar mymmo-api-1.0-SNAPSHOT.jar  


```

### Frontend
```
Mettre la clé de l'API Gmaps dans l' AndroidManifest.xml
S'assurer de bien mettre l'url de l'api backend  dans la classe MapsActivity

Sur l'émulateur paramétré la position courante dans la partie configuration (Location) de l'émulateur
```

## Technologies


### Backend 
Spring boot,
Maven

### Fronend 
android


